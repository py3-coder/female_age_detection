#!/usr/bin/env python
# coding: utf-8

# # Female_Age_Face_Detection
Author :- Saurabh Kumar
# In[1]:


import os
from pathlib import Path
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import tensorflow as tf


# In[2]:


from tensorflow import keras
import cv2
from tensorflow.keras.layers import Dense,Input,Lambda,Flatten,MaxPooling2D,Dropout,Conv2D
from tensorflow.keras.models  import Model
from tensorflow.keras.applications.resnet import preprocess_input
from tensorflow.keras.applications.vgg19 import VGG19
from tensorflow.keras.preprocessing import image
from tensorflow.keras.preprocessing.image import ImageDataGenerator,load_img
from tensorflow.keras.models import Sequential
from glob import glob
from PIL import Image
from IPython.display import Image
import warnings
warnings.filterwarnings("ignore")


# In[3]:


#GPU and CPU
tf.config.list_physical_devices()


# In[4]:


if tf.test.gpu_device_name(): 
    print('Default GPU Device: {}'.format(tf.test.gpu_device_name()))
else:
    print("Please install GPU version of TF")


# In[5]:


print("Num GPUs Available: ", len(tf.config.experimental.list_physical_devices('GPU')))


# In[6]:


pwd


# In[7]:


path='E:\\DataScience\\Data_Center\\Female_Age_Face_Detection'
os.listdir(path)


# In[8]:


print(os.listdir('E:\\DataScience\\Data_Center\\Female_Age_Face_Detection\\Complete_Data'))


# In[9]:


Complete_Data= "E:\\DataScience\\Data_Center\\Female_Age_Face_Data\\Complete_Data\*"


# In[10]:


for dirname, _, filenames in os.walk('E:\\DataScience\\Data_Center\\Female_Age_Face_Data\\Complete_Data'):
    for filename in filenames:
        print(os.path.join(dirname, filename))


# ## Image Female :18 Year

# In[11]:


img_18=[]
img_18_path="E:\\DataScience\\Data_Center\\Female_Age_Face_Detection\\Complete_Data\\18"
for filename in os.listdir(img_18_path):
    img_18.append(os.path.join(img_18_path, filename))

fig = plt.figure(figsize = (20,20))
for i in range(5):
    idx = np.random.randint(0,1000)
    plt.subplot(5,5,i+1)
    image=plt.imread(img_18[idx])
    plt.imshow(image)


# ## Image Female :19 Year

# In[12]:


img_19=[]
img_19_path="E:\\DataScience\\Data_Center\\Female_Age_Face_Detection\\Complete_Data\\19"
for filename in os.listdir(img_18_path):
    img_19.append(os.path.join(img_19_path, filename))
    
fig = plt.figure(figsize = (20,20))
for i in range(5):
    idx = np.random.randint(0,1000)
    plt.subplot(5,5,i+1)
    image=plt.imread(img_19[idx])
    plt.imshow(image)
    


# ## Image Female :20 Year

# In[13]:


img_20=[]
img_20_path="E:\\DataScience\\Data_Center\\Female_Age_Face_Detection\\Complete_Data\\20"
for filename in os.listdir(img_20_path):
    img_20.append(os.path.join(img_20_path, filename))
    
fig = plt.figure(figsize = (20,20))
for i in range(5):
    idx = np.random.randint(0,1000)
    plt.subplot(5,5,i+1)
    image=plt.imread(img_20[idx])
    plt.imshow(image)


# # Image Female :21 Year

# In[14]:


img_21=[]
img_21_path="E:\\DataScience\\Data_Center\\Female_Age_Face_Detection\\Complete_Data\\21"
for filename in os.listdir(img_21_path):
    img_21.append(os.path.join(img_21_path, filename))

fig = plt.figure(figsize = (20,20))
for i in range(5):
    idx = np.random.randint(0,1000)
    plt.subplot(5,5,i+1)
    image=plt.imread(img_20[idx])
    plt.imshow(image)


# In[15]:


for dirname,_ ,filesnames in os.walk("E:\\DataScience\\Data_Center\\Female_Age_Face_Detection\\Complete_Data"):
    for filename in filesnames:
        print(os.path.join(dirname,filename))


# In[18]:


##<<<<<<<<<<<<<<<<<----------Test-Train data seperation from a single folder------->>>>>>>>>>>>>

import os
import shutil

def train_test_split():
    print("Train Test Val Script started")
    #data_csv = pd.read_csv("DataSet_Final.csv") ##Use if you have classes saved in any .csv file

    root_dir ="E:/DataScience/Data_Center/Female_Age_Face_Detection/Data_F"
    classes_dir =['18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49', '50', '51', '52', '53', '54', '55', '56', '57', '58', '59', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '70']

    #for name in data_csv['names'].unique()[:10]:
    #    classes_dir.append(name)

    processed_dir = 'E:/DataScience/Data_Center/Female_Age_Face_Detection/data/'

    val_ratio = 0.10
    test_ratio = 0.20

    for cls in classes_dir:
        # Creating partitions of the data after shuffeling
        print("$$$$$$$ Class Name " + cls + " $$$$$$$")
        src = processed_dir +"//" + cls  # Folder to copy images from

        allFileNames = os.listdir(src)
        np.random.shuffle(allFileNames)
        train_FileNames, val_FileNames, test_FileNames = np.split(np.array(allFileNames),
                                                                  [int(len(allFileNames) * (1 - (val_ratio + test_ratio))),
                                                                   int(len(allFileNames) * (1 - val_ratio)),
                                                                   ])

        train_FileNames = [src + '//' + name for name in train_FileNames.tolist()]
        val_FileNames = [src + '//' + name for name in val_FileNames.tolist()]
        test_FileNames = [src + '//' + name for name in test_FileNames.tolist()]

        print('Total images: '+ str(len(allFileNames)))
        print('Training: '+ str(len(train_FileNames)))
        print('Validation: '+  str(len(val_FileNames)))
        print('Testing: '+ str(len(test_FileNames)))

        # # Creating Train / Val / Test folders (One time use)
        os.makedirs(root_dir + '/train//' + cls)
        os.makedirs(root_dir + '/val//' + cls)
        os.makedirs(root_dir + '/test//' + cls)

        # Copy-pasting images
        for name in train_FileNames:
            shutil.copy(name, root_dir + '/train//' + cls)

        for name in val_FileNames:
            shutil.copy(name, root_dir + '/val//' + cls)

        for name in test_FileNames:
            shutil.copy(name, root_dir + '/test//' + cls)

    print("Train Test Val Script Ended")

train_test_split()


# In[16]:


#path 
train_path ='E:\\DataScience\\Data_Center\\Female_Age_Face_Detection\\Data\\train'
test_path ='E:\\DataScience\\Data_Center\\Female_Age_Face_Detection\\Data\\test'
val_path='E:\\DataScience\\Data_Center\\Female_Age_Face_Detection\\Data\\val'


# In[17]:


#image agumentation for training set

training_gen = ImageDataGenerator(rescale=1./255,
                               rotation_range=10,
                               width_shift_range=.1,
                               height_shift_range=.1,
                               shear_range=0.1,
                               zoom_range=0.1,
                               horizontal_flip=True,
                               fill_mode="nearest")

#image agumentation for testing set
testing_gen =ImageDataGenerator(rescale=1./255)

#image agumentation for validation set
val_gen =ImageDataGenerator(rescale=1./255,
                            rotation_range=10,
                            width_shift_range=.1,
                            height_shift_range=.1,
                            shear_range=0.1,
                            zoom_range=0.1,
                            horizontal_flip=True,
                            fill_mode="nearest")


# In[37]:


#
train_data =training_gen.flow_from_directory('E:\\DataScience\\Data_Center\\Female_Age_Face_Detection\\Data\\train',
                                           target_size=(224,224),batch_size=64,class_mode='categorical')

test_data =testing_gen.flow_from_directory('E:\\DataScience\\Data_Center\\Female_Age_Face_Detection\\Data\\test',
                                           target_size=(224,224),batch_size=64,class_mode='categorical')

valid_data =val_gen.flow_from_directory('E:\\DataScience\\Data_Center\\Female_Age_Face_Detection\\Data\\val',
                                           target_size=(224,224),batch_size=64,class_mode='categorical')


# In[19]:


#train_gen =np.array(train_gen)
#test_gen =np.array(test_gen)
#valid_gen =np.array(val_gen)


# In[50]:


#Model 

model = Sequential([
    #1st conv2d layer  ---3 channnel RGB
    Conv2D(64,(3,3),activation='relu',strides=2,input_shape=(224,224,3)),
    MaxPooling2D(2,2),
    
    #2nd conv2d layer
    Conv2D(128,(3,3),activation='relu'),
    MaxPooling2D(2,2),
    
    #3rd conv2d layer
    Conv2D(256,(3,3),activation='relu'),
    MaxPooling2D(2,2),
    
    #4th conv2d layer
    #Conv2D(512,(3,3),activation='relu'),
    #MaxPooling2D(2,2),
    #Dropout(0.1),
    
    #5th coved layers
    Conv2D(256,(3,3),activation='relu'),
    MaxPooling2D(2,2),
    Dropout(0.09),
    
    Flatten(),
    Dropout(0.1),
    
    #1st dense layer
    Dense(128,activation='relu'),
    
    #2nd dense layer 
    Dense(256,activation ='relu'),
    Dropout(0.1),
    
    #3rd dense layer
    Dense(512,activation ='relu'),
    Dropout(0.4),
    
    #output layer
    Dense(53,activation ='softmax'),
])

model.compile(loss='categorical_crossentropy',optimizer='adam',metrics=['accuracy'])


# In[51]:


model.summary()


# In[52]:


model.layers


# In[53]:


#traning images with own model created
with tf.device('/GPU:0'):
    history =model.fit_generator(
    valid_data,
    validation_data=test_data,
    epochs=5,
    steps_per_epoch =len(valid_data),
    validation_steps=len(test_data)
)


# In[49]:


#traning images with tranfer learning model
with tf.device('/GPU:0'):
    history =model.fit_generator(
    valid_data,
    validation_data=test_data,
    epochs=5,
    steps_per_epoch =len(valid_data),
    validation_steps=len(test_data)
)


# In[22]:


#
Image_size=[224,224]


# In[23]:


#importing the VGG19 and also removing top_layers and we will use pre-trained weight(imagnet weight)
mobilenet =VGG19(include_top=False,input_shape=Image_size +[3],weights='imagenet')


# In[24]:


#need_not to train the weight a we are using pre trained weight
for layer in mobilenet.layers:
  layer.trainable =False


# In[25]:


#fetehcing the folder
fold =glob("E:\\DataScience\\Data_Center\\Female_Age_Face_Detection\\Data\\train\\*")
fold


# In[26]:


#adding output layer
X =Flatten()(mobilenet.output)
out_layers =Dense(len(fold),activation='softmax')(X)


# In[27]:


#creating instance of model
model =Model(inputs=mobilenet.input,outputs=out_layers)
model.summary()


# In[28]:


#compile_model
model.compile(
    optimizer='adam',
     loss='categorical_crossentropy',
     metrics=['accuracy']
)   


# In[38]:


#traning images using cpu ----> transfer learning

history =model.fit_generator(
    valid_data,
    validation_data=test_data,
    epochs=5,
    steps_per_epoch =len(valid_data),
    validation_steps=len(test_data)
)


# In[ ]:


#due to large number to data set only runing for 5 epochs ..to check the rate of change of model learning


# In[ ]:





# In[ ]:




